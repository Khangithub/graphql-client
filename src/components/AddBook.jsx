import React, {useState} from 'react';
import {useQuery, useMutation} from '@apollo/client';
import * as Queries from '../queries/index';

function AddBook() {
  const {loading, error, data} = useQuery(Queries.getAuthorQuery);
  const [addBook, {result}] = useMutation(Queries.addBookMutation);

  const [newBook, setNewBook] = useState({
    name: '',
    genre: '',
    author: '',
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    addBook({
      variables: {
        name: newBook.name,
        genre: newBook.genre,
        authorId: newBook.author,
      },
      refetchQueries: [
        {
          query: Queries.getBooksQuery,
        },
      ],
    });

    console.log(result, 'handleSubmit');
  };

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <div>
      <form action="" id="add-book" onSubmit={handleSubmit}>
        <div className="field">
          <label htmlFor="">Book name:</label>
          <input
            type="text"
            name=""
            id=""
            onChange={(e) => setNewBook({...newBook, name: e.target.value})}
          />
        </div>
        <div className="field">
          <label htmlFor="">Genre:</label>
          <input
            type="text"
            name=""
            id=""
            onChange={(e) => setNewBook({...newBook, genre: e.target.value})}
          />
        </div>
        <div className="field">
          <label htmlFor="">Author:</label>
          <select
            name=""
            id=""
            onChange={(e) => setNewBook({...newBook, author: e.target.value})}
          >
            {data.authors.map(({name, id}) => (
              <option key={id} value={id}>
                {name}
              </option>
            ))}
          </select>
        </div>
        <button type="submit">+</button>
      </form>
    </div>
  );
}

export default AddBook;
