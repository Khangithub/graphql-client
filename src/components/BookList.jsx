import React, {useState} from 'react';
import {useQuery} from '@apollo/client';
import * as Queries from '../queries/index';
import BookDetails from './BookDetails';

const BookList = () => {
  const {loading, error, data} = useQuery(Queries.getBooksQuery);
  const [bookId, setBookId] = useState(null);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <div>
      <ul id="book-list">
        {data.books.map(({name, id}) => (
          <li key={id} onClick={() => setBookId(id)}>
            {name}
          </li>
        ))}
      </ul>
      <BookDetails bookId={bookId} />
    </div>
  );
};

export default BookList;
