import React from 'react';
import {useQuery} from '@apollo/client';
import * as Queries from '../queries/index';

function BookDetails({bookId}) {
  const {loading, error, data} = useQuery(Queries.getBookQuery, {
    variables: {
      id: bookId,
    },
  });

  if (loading) return null;
  if (error) console.log(`Error! ${error}`);

  if (data) {
    console.log(data);
    return (
      <div>
        <div id="book-details">
          <p>Output book details here</p>
        </div>
        <div>
          <h2>{data.book.name}</h2>
          <p>{data.book.genre}</p>
          <p>{data.book.author.name}</p>
          <p>All books by this author:</p>
          <ul className="other-books">
            {data.book.author.books.map((item) => (
              <li key={item.id}>{item.name}</li>
            ))}
          </ul>
        </div>
      </div>
    );
  }

  return <div></div>;
}

export default BookDetails;
